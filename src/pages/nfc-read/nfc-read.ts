import { Component } from '@angular/core';
import {
  AlertController,
  NavController
} from 'ionic-angular';

import { UserLoginPage } from '../user-login/user-login';

import { NfcProvider } from '../../providers/nfc-provider';
import { AdminSession } from '../../providers/admin-session';

@Component({
  selector: 'page-nfc-read',
  templateUrl: 'nfc-read.html'
})
export class NfcReadPage {

  public data:string='';

  constructor(
    public nfcProvider: NfcProvider,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public adminSession: AdminSession
  ) {}

  ionViewDidLoad() {}

  ionViewDidEnter(){
    if( this.adminSession.isLog() == false ){
      this.navCtrl.push(UserLoginPage);
    }
  }

  read(){
    this.nfcProvider.read(
      true,
      10,
      (data) => {
        this.data = data;
      },
      (error) => {
        this.alertCtrl.create({
          title: 'Eye Time',
          subTitle: error,
          buttons: [{ text : "OK"}]
        }).present();
      }
    );
  }

}
