import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';
import {
  NavController,
  LoadingController,
  Loading,
  AlertController,
  Platform
} from 'ionic-angular';

import { SimpleTimer } from 'ng2-simple-timer';
import moment from 'moment';

import { DatabaseProvider } from '../../providers/database-povider';
import { SessionModelProvider } from '../../providers/session-model-provider';
import { LogModelProvider } from '../../providers/log-model-provider';
import { SpeechProvider } from '../../providers/speech-provider';
import { SystemSettings } from '../../providers/system-settings';
import { CommonProvider } from '../../providers/common-provider';
import { LoggerProvider } from '../../providers/logger-provider';

import { TimeOutPage } from '../time-out/time-out';
import { HomePage } from '../home/home';

interface GpsStatus{
  retryGps: boolean,
  continue: boolean,
  gpsFound: boolean
};

@Component({
  selector: 'page-time-in',
  templateUrl: 'time-in.html'
})
export class TimeInPage implements OnInit, OnDestroy{

  public timer:string;
  public nowDate:string;
  public nowTime:string;

  public timerStatus:string;

  public storeType:string = '1';

  public remarks:string = "";

  public gpsLocationRetry: number = 0;

  constructor(
    public platform: Platform,
    public dbProvider: DatabaseProvider,
    public simpleTimer: SimpleTimer,
    public navCtrl: NavController,
    public sessionModelProvider: SessionModelProvider,
    public loadingCtrl: LoadingController,
    public logModelProvider: LogModelProvider,
    public speechProvider: SpeechProvider,
    public systemSettings: SystemSettings,
    public commonProvider: CommonProvider,
    public alertCtrl: AlertController,
    public loggerProvider: LoggerProvider
    ) {

   this.platform.registerBackButtonAction(()=>{
      this.alertCtrl.create({
        title: 'Eye Time',
        message: 'Are you sure you want to close this app?',
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              navigator['app'].exitApp();
            },
          },
          {
            text: 'No',
            handler: () => {
            }
          }
        ]
      }).present();
    });

  }

  ngOnInit(){
    this.simpleTimer.newTimer('1sec',1);
  }
  ngOnDestroy(){
    this.simpleTimer.unsubscribe(this.timer);
  }

  ionViewDidLoad() {
  }
  ionViewDidEnter(){
    this.timer = this.simpleTimer.subscribe('1sec', e => this.timerCallback());

    this.platform.ready().then(() => {
      this.dbProvider.connect()
        .then(
          ()=>{
            this.timerStatus = this.simpleTimer.subscribe('1sec', e => this.timerStatusCallback());
          }
        );
    });
    
  }
  ionViewWillLeave(){
    this.simpleTimer.unsubscribe(this.timer);
  }

  timerCallback(){
    this.nowDate = moment().format("dddd, MMMM DD YYYY");
    this.nowTime = moment().format("hh:mm:ss A");
  }

  timerStatusCallback(){
    if( this.dbProvider.connected && this.systemSettings.databaseInitialized ){
      this.simpleTimer.unsubscribe(this.timerStatus);

      let loading: Loading;
      loading = this.loadingCtrl.create({
        content: `
          <div class="custom-spinner-container">
            <div class="custom-spinner-box"></div>
            Checking status..
          </div>`
      });
      loading.present();
      this.logModelProvider.status()
        .then(
          (result)=>{
            
            if(result==='logout'){
              this.navCtrl.setRoot(TimeOutPage);
            }

            loading.dismiss();
          },
          (error)=>{
            loading.dismiss();

            this.commonProvider.showMessage("Checking status error!.");
          }
        );
    }
  }

  selectStoreType(storeType:string){
    this.storeType = storeType;
  }

  private gpsData:any = null;

  timein(){
    this.gpsLocationRetry = 1;
    this._timein_step1();
  }

  _timein_step1(){

    let loading: Loading;
    loading = this.loadingCtrl.create({
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please wait..
        </div>`
    });
    loading.present();


    this.commonProvider.getLocation(this.systemSettings.settings.gps_timeout)
      .then(
        geoPosition => {
          this.gpsData = geoPosition;

          return Promise.resolve({retryGps: false, continue: true, gpsFound: true});
        },
        error => {
          this.gpsLocationRetry++;
          if(this.gpsLocationRetry<=3){
            this.loggerProvider.log('['+ this.gpsLocationRetry +']Retrying..');

            return Promise.resolve({retryGps: true, continue: true, gpsFound: false});
          }else{
            return Promise.resolve({retryGps: false, continue: true, gpsFound: false});
          }
        }
      )
      .then(
        result => {
          loading.dismiss();

          let res = <GpsStatus> result;

          if( res.retryGps === true ){
            this._timein_step1();
          }else if( res.retryGps === false && res.continue === true ){

            if( res.gpsFound === false ){
              if(this.systemSettings.settings.require_gps){
                this.commonProvider.showMessage('Can\'t locate your current location, please try again.');
              }else{
                this._timein_step2_with_confirmation();
              }
            }else{
              this._timein_step2();
            }
            
          }
          
        }
      );
  }

  _timein_step2_with_confirmation(){
    this.alertCtrl.create({
      title: 'Eye Time',
      message: 'Can\'t locate your current location, are you sure you want to proceed?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {

            this.gpsData = {};
            this.gpsData.coords = {};
            this.gpsData.coords.longitude=0
            this.gpsData.coords.latitude=0;
            this.gpsData.timestamp=0;

            this._timein_step2();
          },
        },
        {
          text: 'No',
          handler: () => {
          }
        }
      ]
    }).present();
  }

  _timein_step2(){
    let loading: Loading;
    
    loading = this.loadingCtrl.create({
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please wait..
        </div>`
    });
    loading.present()
      .then(
        () => {
          
          this.commonProvider.fingerAuth()
            .then(
              success => {
                
                this.commonProvider.readNfc( (this.storeType=="1") )
                  .then(
                    nfcData => {

                      this.saveTimeIn(
                        this.storeType,
                        ''+this.gpsData.coords.longitude,
                        ''+this.gpsData.coords.latitude,
                        this.gpsData.timestamp,
                        nfcData
                      )
                        .then(
                          (result) => {
                            loading.dismiss();

                            // Success
                            let timein = moment(result.entry_timestamp).format("MMM DD YYYY hh:mm:ss A");
                            this.speechProvider.speak('You have successfully Time In');

                            this.commonProvider.showMessage('You have successfully Time In<br/><br/><strong>' + timein + '</strong>');
                            this.navCtrl.setRoot(TimeOutPage);

                          },
                          (error) => {
                            loading.dismiss();
                            this.commonProvider.showMessage(JSON.stringify(error));      
                          }
                        );

                    },
                    error => {
                      loading.dismiss();
                      this.commonProvider.showMessage(JSON.stringify(error));      
                    }
                  );

              },
              error => {
                loading.dismiss();
                this.commonProvider.showMessage(JSON.stringify(error));
              }
            );

        }
      );
  }


  saveTimeIn(
    storeType,
    long,
    lat,
    geoTimestamp,
    storeCode
  ):Promise<any>{

    return new Promise((resolve, reject)=>{
      
      this.logModelProvider.save(
          this.sessionModelProvider.sessionStatus.id,
          'in',
          storeType,
          long,
          lat,
          geoTimestamp,
          storeCode,
          this.remarks
        )
          .then(
            (result)=>{
              resolve(result);
            },
            (error) => {
              reject(error);
            }
          );

    });

  }

  endDay(){
    this.alertCtrl.create({
      title: 'Eye Time',
      message: 'Are you sure you want to proceed?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this._endDay();
          }
        },
        {
          text: 'No',
          handler: () => {
          }
        }
      ]
    }).present();
  }

  _endDay(){
    let loading: Loading;
    loading = this.loadingCtrl.create({
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please wait..
        </div>`
    });
    loading.present();

    this.commonProvider.geoLocationProvider.getLocation(
      this.systemSettings.settings.gps_timeout,
      false,
      (geoPosition) => {

        this.sessionModelProvider.endDay(
          ''+geoPosition.coords.latitude,
          ''+geoPosition.coords.longitude,
          geoPosition.timestamp
        )
          .then(
            (result) => {
              loading.dismiss();

              this.navCtrl.setRoot(HomePage);
            },
            (error) => {
              loading.dismiss();
            }
          );

      },
      (error) => {
        loading.dismiss();
        this.commonProvider.showMessage('Unable to find location, please try again.');
      }
    );
  }

  
}
