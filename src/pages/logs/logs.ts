import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';

import {
  Platform
} from 'ionic-angular';

import { DatabaseProvider } from '../../providers/database-povider';

import moment from 'moment';

 import * as _ from 'lodash';

interface LogData{
  type:string,
  storeType:string,
  storeCode:string,
  entryDateTime:string,
  startDay:string,
  endDay:string,
  uploadTimestamp:string,
  flag:string
};

@Component({
  selector: 'page-logs',
  templateUrl: 'logs.html'
})
export class LogsPage implements OnInit, OnDestroy{

  public logsData: LogData[] = [];
  public logData:any;
  public logDataKeys:any;

  constructor(
    public platform: Platform,
    public dbProvider: DatabaseProvider
  ) {}

  ionViewDidLoad() {
    this.platform.ready().then(() => {
      this.dbProvider.connect()
        .then(
          ()=>{
            this.loadLogData();
          }
        );
    });
  }

  ngOnInit(){
  }

  ngOnDestroy(){

  }

  loadLogData(){

    let sevendays = moment().subtract(7, "days").format('x');

    this.dbProvider.getDb()
      .executeSql(`DELETE FROM "main".sessions WHERE action='end' AND flag=1 AND s_entry_timestamp <= ${sevendays}`, {})
      .then(
        () => {
          this.dbProvider.getDb()
          .executeSql(`DELETE FROM "main".logs WHERE flag=1 AND entry_timestamp <= ${sevendays}`, {})
          .then(
            () => {
              this.getLogs();
            },
            (error) => {
              console.log( JSON.stringify(error) );
            }
          );
        },
        (error) => {
          console.log( JSON.stringify(error) );
        }
      );

    
  }

  getLogs(){
    this.dbProvider.getDb()
      .executeSql(`
      SELECT
        A.*,
        B.s_entry_timestamp,
        B.e_entry_timestamp
      FROM
        "main"."logs" AS A
        INNER JOIN
        "main"."sessions" AS B
        ON(A.session_id=B.id)
      ORDER BY B.id ASC, A.id ASC`, {})
        .then(
          (rs) => {

            for(let i=0; i<rs.rows.length; i++){

              let storeType="";
              if(rs.rows.item(i).store_type==1){
                storeType = 'Store Visit';
              }else if(rs.rows.item(i).store_type==2){
                storeType = 'Meeting';
              }else if(rs.rows.item(i).store_type==3){
                storeType = 'Scouting';
              }else if(rs.rows.item(i).store_type==4){
                storeType = 'Travel';
              }

              let type = "";
              if(rs.rows.item(i).type=="in"){
                type = 'In';
              }else if(rs.rows.item(i).type=="out"){
                type = 'Out';
              }else if(rs.rows.item(i).type=="fout"){
                type = 'F-Out';
              }

              let startDay:string = '';
              if(rs.rows.item(i).s_entry_timestamp){
                startDay = moment(rs.rows.item(i).s_entry_timestamp).format("MMM-DD-YYYY hh:mm:ss a");
              }
              let endDay:string = '';
              if(rs.rows.item(i).e_entry_timestamp){
                endDay = moment(rs.rows.item(i).e_entry_timestamp).format("MMM-DD-YYYY hh:mm:ss a");
              }

              this.logsData.push({
                type: type,
                storeType: storeType,
                storeCode: rs.rows.item(i).store_code,
                entryDateTime: moment(rs.rows.item(i).entry_timestamp).format("MM-DD-YYYY hh:mm:ss a"),
                startDay: startDay,
                endDay: endDay,
                uploadTimestamp: moment(rs.rows.item(i).upload_timestamp).format("MM-DD-YYYY hh:mm:ss a"),
                flag: rs.rows.item(i).flag
              });

            }

            let result = _.groupBy(this.logsData, (value)=>{
              return '<label>Start Day</label> : ' + value.startDay + '<br/><label>End Day</label> : ' +  value.endDay;
            });

            this.logData = result;
            this.logDataKeys = Object.keys(result);

          },
          (error) => {
            console.log( JSON.stringify(error) );
          }
        );
  }

}
