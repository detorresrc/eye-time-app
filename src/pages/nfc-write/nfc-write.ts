import { Component } from '@angular/core';
import {
  AlertController,
  NavController
} from 'ionic-angular';

import { UserLoginPage } from '../user-login/user-login';

import { NfcProvider } from '../../providers/nfc-provider';
import { AdminSession } from '../../providers/admin-session';

@Component({
  selector: 'page-nfc-write',
  templateUrl: 'nfc-write.html'
})
export class NfcWritePage {

  public data:string='';

  constructor(
    public nfcProvider: NfcProvider,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public adminSession: AdminSession
    ) {}

  ionViewDidLoad() {}

  ionViewDidEnter(){
    if( this.adminSession.isLog() == false ){
      this.navCtrl.push(UserLoginPage);
    }
  }

  write(){

    this.data = this.data.trim();

    if( this.data.length == 0 ){
      let alert = this.alertCtrl.create({
        title: 'Eye Time',
        subTitle: 'Please input data!',
        buttons: [{ text : "OK"}]
      });
      alert.present();
      return false;
    }

    this.nfcProvider.checkNfc(true)
      .then(
        (result) => {
          
          this.nfcProvider.write(
            this.data,
            ()=>{
              this.alertCtrl.create({
                title: 'Eye Time',
                subTitle: 'Success',
                buttons: [{ text : "OK"}]
              }).present();
            },
            (error)=>{
              this.alertCtrl.create({
                title: 'Eye Time',
                subTitle: 'Error, please try again!',
                buttons: [{ text : "OK"}]
              }).present();
            }
            );

        },
        (error) => {
          if(error=="NFC_DISABLED"){
            let alert = this.alertCtrl.create({
              title: 'Eye Time',
              subTitle: 'NFC is disabled',
              buttons: [{ text : "OK"},{ text : "Go Setting",
                handler : () => {
                  this.nfcProvider.showSetting();
                }
              }]
            });
            alert.present();
          }else{
            let alert = this.alertCtrl.create({
              title: 'Eye Time',
              subTitle: error,
              buttons: [{ text : "OK"}]
            });
            alert.present();
          }
        }
      );

  }
}
