import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';

import {
  NavController,
  NavParams,
  LoadingController,
  Loading,
  AlertController,
  Platform
} from 'ionic-angular';

import { SimpleTimer } from 'ng2-simple-timer';
import moment from 'moment';

import { LogModelProvider } from '../../providers/log-model-provider';
import { SpeechProvider } from '../../providers/speech-provider';
import { CommonProvider } from '../../providers/common-provider';
import { DatabaseProvider } from '../../providers/database-povider';
import { SessionModelProvider } from '../../providers/session-model-provider';
import { LoggerProvider } from '../../providers/logger-provider';
import { SystemSettings } from '../../providers/system-settings';

import { TimeInPage } from '../time-in/time-in';

interface GpsStatus{
  retryGps: boolean,
  continue: boolean,
  gpsFound: boolean
};

@Component({
  selector: 'page-time-out',
  templateUrl: 'time-out.html'
})
export class TimeOutPage implements OnInit, OnDestroy{

  public forgotToTimeoutStatus:boolean = false;

  public timer:string;
  public nowDate:string;
  public nowTime:string;

  constructor(
    public simpleTimer: SimpleTimer,
    public navCtrl: NavController,
    public navParams: NavParams,
    public logModelProvider: LogModelProvider,
    public speechProvider: SpeechProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public commonProvider: CommonProvider,
    public dbProvider: DatabaseProvider,
    public platform: Platform,
    public sessionModelProvider: SessionModelProvider,
    public loggerProvider: LoggerProvider,
    public systemSettings: SystemSettings
    ) {

    this.platform.registerBackButtonAction(()=>{
      this.alertCtrl.create({
        title: 'Eye Time',
        message: 'Are you sure you want to close this app?',
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              navigator['app'].exitApp();
            },
          },
          {
            text: 'No',
            handler: () => {
            }
          }
        ]
      }).present();
    });

  }

  ngOnInit(){
    this.simpleTimer.newTimer('1sec',1);
    this.platform.ready().then(() => {
      this.dbProvider.connect()
        .then(
          ()=>{
          }
        );
    });
  }

  ngOnDestroy(){

  }

  ionViewDidLoad() {
  }

  ionViewDidEnter(){
    this.timer = this.simpleTimer.subscribe('1sec', e => this.timerCallback());
  }

  ionViewWillLeave(){
    this.simpleTimer.unsubscribe(this.timer);
  }

  timerCallback(){
    this.nowDate = moment().format("dddd, MMMM DD YYYY");
    this.nowTime = moment().format("hh:mm:ss A");
  }

  timeout(){
    this.forgotToTimeoutStatus = false;
    this._timeout_step1();
  }

  forgotToTimeout(){
    this.forgotToTimeoutStatus = true;
    this._timeout_step1();
  }

  private gpsLocationRetry: number = 0;
  private gpsData:any = null;
  _timeout_step1(){
    let loading: Loading;
    loading = this.loadingCtrl.create({
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please wait..
        </div>`
    });
    loading.present();


    this.commonProvider.getLocation(this.systemSettings.settings.gps_timeout)
      .then(
        geoPosition => {
          this.gpsData = geoPosition;

          return Promise.resolve({retryGps: false, continue: true, gpsFound: true});
        },
        error => {
          this.gpsLocationRetry++;
          if(this.gpsLocationRetry<=3){
            this.loggerProvider.log('['+ this.gpsLocationRetry +']Retrying..');

            return Promise.resolve({retryGps: true, continue: true, gpsFound: false});
          }else{
            return Promise.resolve({retryGps: false, continue: true, gpsFound: false});
          }
        }
      )
      .then(
        result => {
          loading.dismiss();

          let res = <GpsStatus> result;

          if( res.retryGps === true ){
            this._timeout_step1();
          }else if( res.retryGps === false && res.continue === true ){

            if( res.gpsFound === false ){
              if(this.systemSettings.settings.require_gps){
                this.commonProvider.showMessage('Can\'t locate your current location, please try again.');
              }else{
                this._timeout_step2_with_confirmation();
              }
            }else{
              this._timeout_step2();
            }
            
          }
          
        }
      );
  }

  _timeout_step2_with_confirmation(){
    this.alertCtrl.create({
      title: 'Eye Time',
      message: 'Can\'t locate your current location, are you sure you want to proceed?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {

            this.gpsData = {};
            this.gpsData.coords = {};
            this.gpsData.coords.longitude=0
            this.gpsData.coords.latitude=0;
            this.gpsData.timestamp=0;

            this._timeout_step2();
          },
        },
        {
          text: 'No',
          handler: () => {
          }
        }
      ]
    }).present();
  }

  _timeout_step2(){
    let loading: Loading;
    
    loading = this.loadingCtrl.create({
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please wait..
        </div>`
    });
    loading.present()
      .then(
        () => {
          
          this.commonProvider.fingerAuth()
            .then(
              success => {
                
                // Read NFC
                let nfcRequired:boolean = true;
                if( this.forgotToTimeoutStatus == true || this.logModelProvider.logStatus.store_type != "1" ){
                    nfcRequired = false;
                }

                this.commonProvider.readNfc( nfcRequired )
                  .then(
                    nfcData => {

                      if( nfcRequired == true && nfcData != this.logModelProvider.logStatus.store_code ){
                          loading.dismiss();
                          this.commonProvider.showMessage("Invalid tag!"); 
                      }else{
                        this.saveTimeOut(
                          this.logModelProvider.logStatus.store_type,
                          ''+this.gpsData.coords.longitude,
                          ''+this.gpsData.coords.latitude,
                          this.gpsData.timestamp,
                          nfcData
                        )
                          .then(
                            (result) => {
                              loading.dismiss();
                              // Success
                              let timeout = moment(result.entry_timestamp).format("MMM DD YYYY hh:mm:ss A");
                              this.speechProvider.speak('You have successfully Time Out');

                              this.commonProvider.showMessage('You have successfully Time Out<br/><br/><strong>' + timeout + '</strong>');
                              this.navCtrl.setRoot(TimeInPage);

                            },
                            (error) => {
                              loading.dismiss();
                              this.commonProvider.showMessage(error);      
                            }
                          );
                      }

                    },
                    error => {
                      loading.dismiss();
                      this.commonProvider.showMessage(error);      
                    }
                  );

              },
              error => {
                loading.dismiss();
                this.commonProvider.showMessage(error);
              }
            );

        }
      );
  }

  saveTimeOut(
    storeType,
    long,
    lat,
    geoTimestamp,
    storeCode
  ):Promise<any>{

    return new Promise((resolve, reject)=>{

      let type:string = 'out';
      if(this.forgotToTimeoutStatus == true){
        type='fout';
      }

      this.logModelProvider.save(
          this.sessionModelProvider.sessionStatus.id,
          type,
          storeType,
          long,
          lat,
          geoTimestamp,
          storeCode
        )
          .then(
            (result)=>{
              resolve(result);
            },
            (error) => {
              reject(error);
            }
          );

    });

  }
}
