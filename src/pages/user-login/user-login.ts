import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ViewController, Platform } from 'ionic-angular';

import { AdminSession } from '../../providers/admin-session';

import { HomePage } from '../home/home';


@Component({
  selector: 'page-user-login',
  templateUrl: 'user-login.html'
})
export class UserLoginPage {

  public userCredentials = {username: '', password: ''};

  constructor(
    public platform: Platform,
    public navCtrl: NavController,
    public navParams: NavParams,
    public adminSession: AdminSession,
    public alertCtrl: AlertController,
    public viewCtrl: ViewController
    ) {

      this.platform.registerBackButtonAction(()=>{
        this.navCtrl.setRoot(HomePage);
      });

    }

  ionViewDidLoad() {
    this.viewCtrl.showBackButton(false);
  }

  login(){
    this.adminSession.login(
      this.userCredentials.username,
      this.userCredentials.password,
      (userData) => {
        this.adminSession.user = userData;

        this.navCtrl.pop();
      },
      () => {
        let alert = this.alertCtrl.create({
          title: 'Eye Time',
          subTitle: 'Invalid username or password!',
          buttons: [{ text : "OK"}]
        });
        alert.present();
      }
    );
  }

  backToHome(){
    this.navCtrl.setRoot(HomePage);
  }

}
