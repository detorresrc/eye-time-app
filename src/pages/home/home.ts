import {
  Component,
  OnInit,
  OnDestroy
} from '@angular/core';

import {
  Platform,
  NavController,
  AlertController,
  LoadingController,
  Loading,
  Events
} from 'ionic-angular';

import { SimpleTimer } from 'ng2-simple-timer';

import moment from 'moment';

import { InitializeDatabase } from '../../providers/initialize-database';
import { GeoLocationProvider } from '../../providers/geo-location-provider';
import { SystemSettings } from '../../providers/system-settings';
import { SystemSettingInitializer } from '../../providers/system-setting-initializer';
import { SessionModelProvider } from '../../providers/session-model-provider';
import { DatabaseProvider } from '../../providers/database-povider';
import { FingerPrintManager } from '../../providers/finger-print-manager';
import { CommonProvider } from '../../providers/common-provider';
import { DataUploaderProvider } from '../../providers/data-uploader-provider';
import { DataJanitorProvider } from '../../providers/data-janitor-provider';

import { TimeInPage } from '../time-in/time-in';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit, OnDestroy{

  private timer: string;
  public nowDate: string;
  public nowTime: string;

  public timerStatus:string;

  public startDayEnable:boolean = false;

  constructor(
    public platform: Platform,
    public simpleTimer: SimpleTimer,
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public initializeDatabase: InitializeDatabase,
    public geoLocationProvider: GeoLocationProvider,
    public systemSettings: SystemSettings,
    public sessionModelProvider: SessionModelProvider,
    public loadingCtrl: LoadingController,
    public dbProvider: DatabaseProvider,
    public events: Events,
    public fingerPrintManager: FingerPrintManager,
    public commonProvider: CommonProvider,
    public dataUploaderProvider: DataUploaderProvider,
    public systemSettingInitializer: SystemSettingInitializer,
    public dataJanitor: DataJanitorProvider
    ) {
    
    this.platform.registerBackButtonAction(()=>{
      this.alertCtrl.create({
        title: 'Eye Time',
        message: 'Are you sure you want to close this app?',
        buttons: [
          {
            text: 'Yes',
            handler: () => {
              navigator['app'].exitApp();
            },
          },
          {
            text: 'No',
            handler: () => {
            }
          }
        ]
      }).present();
    });
  }

  

  ngOnInit(){
    this.platform.ready().then(() => {

      if( this.systemSettings.allowedAppByFingerScan == false ){
        this.fingerPrintManager.verify()
          .then(
            ()=>{
              this.systemSettings.allowedAppByFingerScan = true;
            },
            (error) => {
              this.platform.exitApp(); 
            }
          )
      }

      this.initializeDatabase.init()
        .then(
          success => {
            this.startDayEnable = true;

            //this.dataJanitor.clean();

            this.systemSettingInitializer.init()
              .then(
                success => {
                  this.dataUploaderProvider.start();
                }
              );
          },
          error => {
            let alert = this.alertCtrl.create({
              subTitle : "Database initialization failed.<br/><br/>Error Messsage: " + error,
              buttons: [{ text : "OK"}]
            });
            alert.present();
          }
        );
      
    });
    
    this.simpleTimer.newTimer('1sec',1);
  }

  ngOnDestroy(){
    this.simpleTimer.unsubscribe(this.timer);
    this.simpleTimer.unsubscribe(this.timerStatus);
  }

  ionViewDidLoad() {
  }

  ionViewDidEnter(){
    this.timer = this.simpleTimer.subscribe('1sec', e => this.timerCallback());
    this.timerStatus = this.simpleTimer.subscribe('1sec', e => this.timerStatusCallback());
  }

  ionViewWillLeave(){
    this.simpleTimer.unsubscribe(this.timer);
    this.simpleTimer.unsubscribe(this.timerStatus);
  }

  timerCallback(){
    this.nowDate = moment().format("dddd, MMMM DD YYYY");
    this.nowTime = moment().format("hh:mm:ss A");
  }

  timerStatusCallback(){
    if( this.startDayEnable && this.systemSettings.databaseInitialized ){
      let loading: Loading;
      loading = this.loadingCtrl.create({
        content: `
          <div class="custom-spinner-container">
            <div class="custom-spinner-box"></div>
            Checking status..
          </div>`
      });
      loading.present();
      this.sessionModelProvider.status()
        .then(
          (result)=>{
            
            if(result==='end'){
              this.navCtrl.setRoot(TimeInPage);
            }else if(result==='start'){
              this.startDayEnable = true;
            }

            loading.dismiss();
            this.simpleTimer.unsubscribe(this.timerStatus);
          },
          (error)=>{
            loading.dismiss();

            this.alertCtrl.create({
              subTitle : "Checking status error!.",
              buttons: [{ text : "OK"}]
            })
            .present();
          }
        );
    }
  }

  startDay(){
    
    this.sessionModelProvider.safeToStartDate()
      .then(
        (result) => {

          if( result.status == false ){
            this.commonProvider.showMessage('End of Day has been processed. No transaction shall be allowed anymore.');
          }else{

            this.alertCtrl.create({
              title: 'Eye Time',
              message: 'Are you sure you want to proceed?',
              buttons: [
                {
                  text: 'Yes',
                  handler: () => {
                    this._startDay();
                  }
                },
                {
                  text: 'No',
                  handler: () => {
                  }
                }
              ]
            }).present();

          }

        },
        error => {
          this.commonProvider.showMessage('An error occured, please try again.');
        }
      );
      
  }

  _startDay(){
    let loading: Loading;
    loading = this.loadingCtrl.create({
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please wait..
        </div>`
    });
    loading.present();
    
    this.geoLocationProvider.getLocation(
      this.systemSettings.settings.gps_timeout,
      false,
      (geoPosition) => {

        this.sessionModelProvider.startDay(
          ''+geoPosition.coords.latitude,
          ''+geoPosition.coords.longitude,
          geoPosition.timestamp
        )
          .then(
            (result) => {
              loading.dismiss();

              this.navCtrl.setRoot(TimeInPage);
            },
            (error) => {
              loading.dismiss();
              this.commonProvider.showMessage( error.message );
            }
          );

      },
      (error) => {
        loading.dismiss();
        this.commonProvider.showMessage('Unable to find location, please try again.');
      }
    );
  }

}
