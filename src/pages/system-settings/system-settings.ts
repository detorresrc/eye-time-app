import { Component } from '@angular/core';
import {
  NavController,
  LoadingController
} from 'ionic-angular';



import { DatabaseProvider } from '../../providers/database-povider';
import { LoggerProvider } from '../../providers/logger-provider';
import { CommonProvider } from '../../providers/common-provider';
import { AdminSession } from '../../providers/admin-session';

import { HomePage } from '../home/home';
import { UserLoginPage } from '../user-login/user-login';

interface SystemSettingModel{
  data_upload_retry: string,
  data_upload_api_url: string,
  gps_timeout: string,
  nfc_read_timeout: string,
  require_gps: number
};

@Component({
  selector: 'page-system-settings',
  templateUrl: 'system-settings.html'
})
export class SystemSettingsPage {

  public setting:SystemSettingModel = {
    data_upload_retry: '',
    data_upload_api_url:'',
    gps_timeout: '',
    nfc_read_timeout: '',
    require_gps: 0
  };

  public found:boolean = false;;

  constructor(
    public navCtrl: NavController,
    public dbProvider: DatabaseProvider,
    public loggerProvider: LoggerProvider,
    public loadingCtrl: LoadingController,
    public commonProvider: CommonProvider,
    public adminSession: AdminSession
    ) {

    }

  ionViewDidEnter(){
    if( this.adminSession.isLog() == false ){
      this.navCtrl.push(UserLoginPage);
    }
  }

  ionViewDidLoad() {
    let loading = this.loadingCtrl.create({
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please wait..
        </div>`
    });
    loading.present();

    this.dbProvider.connect()
      .then(

        (db) => {

          db.executeSql(`SELECT * FROM "main"."settings"`, {})
            .then(
              res => {

                if( res.rows.length == 0 ){
                  this.commonProvider.showMessage('No setting found!');
                }else{
                  let row = res.rows.item(0);
                  
                  this.setting.data_upload_retry = row.data_upload_retry,
                  this.setting.data_upload_api_url = row.data_upload_api_url;
                  this.setting.gps_timeout = row.gps_timeout,
                  this.setting.nfc_read_timeout = row.nfc_read_timeout;
                  this.setting.require_gps = row.require_gps;

                  this.found = true;

                }

                loading.dismiss();

              },
              error => {
                this.commonProvider.showMessage(error);
                loading.dismiss();
              }
            )

        },
        error => {
          this.commonProvider.showMessage(error);
          loading.dismiss();
        }

      );
  }

  save(){
    this.loggerProvider.log( this.setting );

    let loading = this.loadingCtrl.create({
      content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Saving, please wait..
        </div>`
    });
    loading.present();

    this.dbProvider.connect()
      .then(

        (db) => {

          db.executeSql(`REPLACE INTO "main"."settings"(id, data_upload_retry, data_upload_api_url, gps_timeout, nfc_read_timeout, require_gps) VALUES(?,?,?,?,?,?)`, [
            1,
            this.setting.data_upload_retry,
            this.setting.data_upload_api_url,
            this.setting.gps_timeout,
            this.setting.nfc_read_timeout,
            ( (this.setting.require_gps) ? 1 : 0  )
          ])
            .then(
              res => {

                loading.dismiss();
                this.commonProvider.showMessage('Setting successfully saved.<br/><br/>Please <strong>restart</strong> app to take effect the changes.');

              },
              error => {

                this.commonProvider.showMessage(error);
                loading.dismiss();

              }
            )

        },
        error => {

          this.commonProvider.showMessage(error);
          loading.dismiss();
        
        }

      );
  }

  backToHome(){
    this.navCtrl.setRoot(HomePage);
  }

  public onChange(){
  }

}
