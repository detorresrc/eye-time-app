import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import { UserLoginPage } from '../pages/user-login/user-login';
import { TimeInPage } from '../pages/time-in/time-in';
import { TimeOutPage } from '../pages/time-out/time-out';
import { NfcReadPage } from '../pages/nfc-read/nfc-read';
import { NfcWritePage } from '../pages/nfc-write/nfc-write';
import { LogsPage } from '../pages/logs/logs';
import { SystemSettingsPage } from '../pages/system-settings/system-settings';

import { InitializeDatabase } from '../providers/initialize-database';
import { AdminSession } from '../providers/admin-session'
import { SystemSettings } from '../providers/system-settings';
import { FingerPrintManager } from '../providers/finger-print-manager';
import { DatabaseProvider } from '../providers/database-povider';
import { GeoLocationProvider } from '../providers/geo-location-provider';
import { NfcProvider } from '../providers/nfc-provider';
import { SessionModelProvider } from '../providers/session-model-provider';
import { LogModelProvider } from '../providers/log-model-provider';
import { SpeechProvider } from '../providers/speech-provider';
import { CommonProvider } from '../providers/common-provider';
import { LoggerProvider } from '../providers/logger-provider';
import { SystemSettingInitializer } from '../providers/system-setting-initializer';

import { DataUploaderProvider } from '../providers/data-uploader-provider';
import { DataJanitorProvider } from '../providers/data-janitor-provider';

import { SimpleTimer } from 'ng2-simple-timer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    UserLoginPage,
    TimeInPage,
    TimeOutPage,
    NfcReadPage,
    NfcWritePage,
    LogsPage,
    SystemSettingsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    UserLoginPage,
    TimeInPage,
    TimeOutPage,
    NfcReadPage,
    NfcWritePage,
    LogsPage,
    SystemSettingsPage
  ],
  providers: [
    {
      provide: ErrorHandler, useClass: IonicErrorHandler
    },
    AdminSession,
    InitializeDatabase,
    SystemSettings,
    FingerPrintManager,
    DatabaseProvider,
    SimpleTimer,
    GeoLocationProvider,
    NfcProvider,
    SessionModelProvider,
    LogModelProvider,
    SpeechProvider,
    CommonProvider,
    DataUploaderProvider,
    LoggerProvider,
    SystemSettingInitializer,
    DataJanitorProvider
  ]
})
export class AppModule {}