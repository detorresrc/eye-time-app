import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { HomePage } from '../pages/home/home';
import { NfcReadPage } from '../pages/nfc-read/nfc-read';
import { NfcWritePage } from '../pages/nfc-write/nfc-write';
import { LogsPage } from '../pages/logs/logs';
import { SystemSettingsPage } from '../pages/system-settings/system-settings';
import { AdminSession } from '../providers/admin-session';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage = HomePage;

  private pages: Array<{title: string, component: any, icon: string}>;

  constructor(
    platform: Platform,
    private adminSession: AdminSession) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });

    this.pages = [
      { title: 'Home', component: HomePage, icon: 'home'}
      ,{ title: 'NFC - Write', component: NfcWritePage, icon: 'download' }
      ,{ title: 'NFC - Read', component: NfcReadPage, icon: 'document' }
      ,{ title: 'Logs', component: LogsPage, icon: 'create' }
      ,{ title: 'Settings', component: SystemSettingsPage, icon: 'ios-settings' }
    ];
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }

  minimizeApp(){
    window['plugins'].appMinimize.minimize();
  }

  logout(){
    this.adminSession.logout();

    this.nav.setRoot(HomePage);
  }
}
