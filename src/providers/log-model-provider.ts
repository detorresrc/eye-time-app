import { Injectable } from '@angular/core';

import { Device } from 'ionic-native';

import { DatabaseProvider } from './database-povider';

import moment from 'moment';

@Injectable()
export class LogModelProvider {

  public logStatus:any=null;

  constructor(
    public dbProvider: DatabaseProvider
  ) {
  }

  save(
    session_id,
    type,
    store_type,
    long,
    lat,
    gps_timestamp,
    store_code,
    remarks?:string
  ):Promise<any>{

    return new Promise((resolve, reject)=>{

      let timestamp:number = parseInt(moment().format('x'));

      this.dbProvider.getDb()
        .executeSql('INSERT INTO "main"."logs"(session_id, store_type, type, long, lat, gps_timestamp, entry_timestamp, entry_date, store_code, phone_id, remarks)\
        VALUES(?,?,?,?,?,?,?,?,?,?,?)',
          [
            session_id,
            store_type,
            type,
            long,
            lat,
            gps_timestamp,
            timestamp,
            moment(timestamp).format('YYYY-MM-DD'),
            store_code,
            Device.uuid,
            remarks
          ]
        )
          .then(
            (result)=>{

              return this.getTimeIn(result.insertId)
                .then(
                  (data)=>{
                    this.logStatus = data;
                    resolve(data);
                  },
                  (error) => {
                    reject(error);
                  }
                );
            },
            (error) => {
              reject(error);
            }
          );
    });

  }

  getTimeIn(log_id:number){

    return new Promise((resolve, reject) => {

      this.dbProvider.getDb()
        .executeSql('SELECT * FROM "main"."logs" WHERE id=?',[log_id])
          .then(
            (result)=>{

              if( result.rows.length == 0 ){
                reject('Log data not found!');
              }else{
                let data:any = result.rows.item(0);
                data.gps_timestamp_formated = moment(data.gps_timestamp).format("MMM DD YYYY hh:mm:ss A");
                data.entry_timestamp_formated = moment(data.entry_timestamp).format("MMM DD YYYY hh:mm:ss A");

                if( data.store_type == "1" ){
                  data.store = "Store Visit";
                }else if( data.store_type == "2" ){
                  data.store = "Meeting";
                }else if( data.store_type == "3" ){
                  data.store = "Scouting";
                }else if( data.store_type == "4" ){
                  data.store = "Travel";
                }else{
                  data.store = "";
                }

                resolve( data );
              }

            },
            (error) => {
              reject(error.message);
            }
          );

    });

  }

  // Return string login or logout
  status():Promise<any>{
    return new Promise((resolve, reject)=>{

      this.dbProvider.getDb()
        .executeSql('SELECT * FROM "main"."logs" ORDER BY id DESC LIMIT 1',{})
          .then(
            (result)=>{

              if( result.rows.length == 0 ){
                resolve('login');
              }else{
                this.logStatus = result.rows.item(0);

                this.logStatus.gps_timestamp_formated = moment(this.logStatus.gps_timestamp).format("MMM DD YYYY hh:mm:ss A");
                this.logStatus.entry_timestamp_formated = moment(this.logStatus.entry_timestamp).format("MMM DD YYYY hh:mm:ss A");

                if( this.logStatus.store_type == "1" ){
                  this.logStatus.store = "Store Visit";
                }else if( this.logStatus.store_type == "2" ){
                  this.logStatus.store = "Meeting";
                }else if( this.logStatus.store_type == "3" ){
                  this.logStatus.store = "Scouting";
                }else if( this.logStatus.store_type == "4" ){
                  this.logStatus.store = "Travel";
                }else{
                  this.logStatus.store = "";
                }

                if( result.rows.item(0).type == 'in' ){
                  resolve('logout');
                }else if( result.rows.item(0).type == 'out' || result.rows.item(0).type == 'fout' ){
                  resolve('login');
                }else{
                  reject(false);
                }
              }

            },
            (error) => {
              reject(false);
            }
          );

    });
  }

}
