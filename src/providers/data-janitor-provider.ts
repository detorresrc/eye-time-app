import { Injectable } from '@angular/core';

import moment from 'moment';
import { DatabaseProvider } from './database-povider';
import { LoggerProvider as Log } from './logger-provider';

@Injectable()
export class DataJanitorProvider {

  constructor(
    public dbProvider: DatabaseProvider,
    public log: Log
  ) {
  }

  clean(){
    this.dbProvider.connect()
      .then(
        db => {
          db.transaction((trx)=>{
            
            this.log.log( 'Data Janitor Process - Started' );

            let sevendays = moment().subtract(7, "days").format('x');

            trx.executeSql(`DELETE FROM "main".sessions WHERE action='end' AND flag=1 AND s_entry_timestamp <= ${sevendays}`, {});
            trx.executeSql(`DELETE FROM "main".logs WHERE flag=1 AND entry_timestamp <= ${sevendays}`, {});

          })
            .then(
              success => {
                this.log.log( 'Data Janitor Process - Done' );
              },
              error => {
                this.log.error( JSON.stringify(error) );
              }
            );
        }
      );
  }

}
