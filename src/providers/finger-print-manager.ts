import { Injectable } from '@angular/core';

import {
  AndroidFingerprintAuth
} from 'ionic-native';

import { SystemSettings } from '../providers/system-settings';

@Injectable()
export class FingerPrintManager {

  constructor(
    public systemSettings: SystemSettings
  ) {
  }

  verify():Promise<any>{

    return new Promise((resolve, reject)=>{

      AndroidFingerprintAuth.isAvailable()
        .then((result)=> {
          if(result.isAvailable){
            AndroidFingerprintAuth.encrypt(this.systemSettings.fingerPrintConfig)
              .then(result => {
                if (result.withFingerprint) {
                  resolve({supported: true, status: true});        
                }else{
                  reject('Fingerprint authentication failed!');
                };
              })
              .catch(error => {
                if (error === "Cancelled") {
                  reject('Fingerprint authentication cancelled!');
                } else{
                  reject(error);
                }
              });
          } else {
            resolve({supported: false, status: true});
          }
        })
        .catch(error => reject(error));

    });
    
  }

}
