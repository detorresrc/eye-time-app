import { Injectable } from '@angular/core';

import {
  AlertController
} from 'ionic-angular';

import { FingerPrintManager } from './finger-print-manager';
import { GeoLocationProvider } from './geo-location-provider';
import { NfcProvider } from './nfc-provider';
import { SystemSettings } from './system-settings';

@Injectable()
export class CommonProvider {

  constructor(
    public alertCtrl: AlertController,
    public fingerPrintManager: FingerPrintManager,
    public geoLocationProvider: GeoLocationProvider,
    public nfcProvider: NfcProvider,
    public systemSettings: SystemSettings
  ) {
  }

  showMessage(msg:string){
    this.alertCtrl.create({
      subTitle : msg,
      buttons: [{ text : "OK"}]
    }).present();
  }

  fingerAuth():Promise<any>{
    return this.fingerPrintManager.verify();
  }

  getLocation(timeout:number=30):Promise<any>{
    return new Promise((resolve, reject)=>{

      this.geoLocationProvider.getLocation(
        timeout,
        false,
        (geoPosition) => {
          resolve(geoPosition);
        },
        (error) => {
          reject("Can't get current location, please try again!");
        }
      );

    });
  }

  readNfc(nfcRequired:boolean):Promise<any>{
    let promise:Promise<any>;

    if( nfcRequired == true ){
      promise = new Promise((resolve, reject)=>{

          this.nfcProvider.read(
            true,
            this.systemSettings.settings.nfc_read_timeout,
            (result) => {
              resolve(result);
            },
            (error) => {
              reject(error)
            }
          );

        });
    }else{
      promise = new Promise((resolve, reject)=>{
          resolve('');
        });
    }
    return promise;
  }

}
