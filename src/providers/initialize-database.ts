import { Injectable } from '@angular/core';

import {
  LoadingController
} from 'ionic-angular';

import { DatabaseProvider } from './database-povider';
import { SystemSettings } from './system-settings';

@Injectable()
export class InitializeDatabase {

  constructor(
    public loadingController:LoadingController,
    public databaseProvider: DatabaseProvider,
    public systemSettings: SystemSettings
  ) {
  }

  init():Promise<any>{

    let loader;
    

    return new Promise((resolve, reject)=>{
      if(this.systemSettings.databaseInitialized==true){
        resolve(true);
      }else{

        loader = this.loadingController.create({
          content: `
          <div class="custom-spinner-container">
            <div class="custom-spinner-box"></div>
            Please wait, checking database..
          </div>`
        });
        loader.present();

        this.databaseProvider.connect()
        .then(
          db => {
            this.check()
              .then(

                status => {
                  if( status === true ){
                    return Promise.resolve(true);
                  }else{
                    return this.create();
                  }
                },
                error => {
                  loader.dismiss();
                  reject(error);
                }
              )
              .then(
                success => {
                  this.systemSettings.databaseInitialized = true;
                  loader.dismiss();
                  resolve(true);
                },
                error => {
                  loader.dismiss();
                  reject(error);
                }
              );
          },
          error => {
            loader.dismiss();
            reject(error);
          }
        );
      }
    });
  }

  check():Promise<any>{

    return new Promise( (resolve, reject) => {

      let db = this.databaseProvider.getDb();
      
      db.transaction((trx)=>{

        trx.executeSql('SELECT * FROM users LIMIT 1');
        trx.executeSql('SELECT * FROM sessions LIMIT 1');
        trx.executeSql('SELECT * FROM logs LIMIT 1');
        trx.executeSql('SELECT * FROM settings LIMIT 1');

      })
      .then(
          () => {
            resolve(true);
          },
          (err) => {
            resolve(false);
        });

    });
  }

  create():Promise<any>{

    return new Promise( (resolve, reject) => {

      let db = this.databaseProvider.getDb();
      db.transaction((trx)=>{
          trx.executeSql(`CREATE TABLE "main"."users" (
"username"  TEXT(50) NOT NULL,
"password"  TEXT(100) NOT NULL,
"active"  INTEGER NOT NULL,
PRIMARY KEY ("username" ASC)
);`);
          trx.executeSql('INSERT INTO users(username, password, active) VALUES(\'admin\', \'0c5fb977d32fce1cb3828d248fa15a5f\', 1);');

          trx.executeSql(`CREATE TABLE "main"."sessions" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT,
"uuid" TEXT(36),
"action" TEXT(5),
"s_long" TEXT(20),
"s_lat" TEXT(20),
"s_gps_timestamp" INTEGER,
"s_entry_timestamp" INTEGER,
"s_entry_date" TEXT(10),
"e_long" TEXT(20),
"e_lat" TEXT(20),
"e_gps_timestamp" INTEGER,
"e_entry_timestamp" INTEGER,
"e_entry_date" TEXT(10),
"phone_id" TEXT(30),
"flag" INTEGER DEFAULT 0,
"upload_timestamp" INTEGER,
CONSTRAINT uuid_unique UNIQUE (uuid)
);`);

        trx.executeSql(`CREATE INDEX "main"."idx_session_phone_id"
ON "sessions"
("phone_id" ASC);`);

        trx.executeSql(`CREATE INDEX "main"."idx_session_action"
ON "sessions"
("action" ASC);`);

          trx.executeSql(`CREATE INDEX "main"."idx_session_s_entry_date"
ON "sessions"
("s_entry_date" ASC);`);

          trx.executeSql(`CREATE INDEX "main"."idx_session_e_entry_date"
ON "sessions"
("e_entry_date" ASC);`);

          trx.executeSql(`CREATE INDEX "main"."idx_session_type"
ON "sessions"
("idx_type" ASC);`);

          trx.executeSql(`CREATE TABLE "main"."logs" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT,
"session_id" INTEGER,
"store_type" INTEGER,
"type" TEXT(5),
"remarks" TEXT(500),
"long" TEXT(20),
"lat" TEXT(20),
"gps_timestamp" INTEGER,
"entry_timestamp" INTEGER,
"entry_date" TEXT(10),
"store_code" TEXT(20),
"phone_id" TEXT(30),
"flag" INTEGER DEFAULT 0,
"upload_timestamp" INTEGER
);`);

        trx.executeSql(`CREATE INDEX "main"."idx_logs_session_id"
ON "logs"
("session_id" ASC);`);

        trx.executeSql(`CREATE INDEX "main"."idx_logs_entry_date"
ON "logs"
("idx_entry_date" ASC);`);

        trx.executeSql(`CREATE INDEX "main"."idx_logs_type"
ON "logs"
("idx_type" ASC);`);

        trx.executeSql(`CREATE TABLE "main"."settings" (
"id" INTEGER PRIMARY KEY AUTOINCREMENT,
"data_upload_retry" INTEGER,
"data_upload_api_url" TEXT(250),
"gps_timeout" INTEGER,
"nfc_read_timeout" INTEGER,
"require_gps" INTEGER
);`);

        trx.executeSql(`INSERT INTO 
        "main"."settings"(
          id,
          data_upload_retry, 
          data_upload_api_url, 
          gps_timeout, 
          nfc_read_timeout,
          require_gps
        ) 
        VALUES(
          1, 
          60, 
          'http://202.128.49.237/eyetime.php', 
          15, 
          15,
          1
        )
        `);

        })
        .then(
            () => {
              resolve(true);
            },
            (err) => {
              reject(err.message);
          });

    });
  }

}