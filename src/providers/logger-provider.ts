import { Injectable } from '@angular/core';

@Injectable()
export class LoggerProvider {

  constructor() {
  }

  error(message:any):void{
    let m:any = message;

    if(typeof message === 'object' || typeof message === 'function'){
      m = JSON.stringify(message);
    }
    console.error( m );
  }

  log(message:any):void{
    let m:any = message;

    if(typeof message === 'object' || typeof message === 'function'){
      m = JSON.stringify(message);
    }
    console.log( m );
  }

  warn(message:any):void{
    let m:any = message;

    if(typeof message === 'object' || typeof message === 'function'){
      m = JSON.stringify(message);
    }
    console.warn( m );
  }

}
