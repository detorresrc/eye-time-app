import { Injectable } from '@angular/core';

import { DatabaseProvider } from './database-povider';
import {Md5} from 'ts-md5/dist/md5';

@Injectable()
export class AdminSession {

  public user: any = null;

  constructor(
    public databaseProvider: DatabaseProvider
  ) {
    
  }

  isLog(){
    if(!this.user){
      return false;
    }else{
      return true;
    }
  }

  logout(){
    this.user = null;
  }

  login(
    username:string,
    password:string,
    callbackSuccess?:(userData:any)=>void,
    callbackError?:()=>void
    ){

    this.databaseProvider.getDb()
      .executeSql("SELECT * FROM \"main\".\"users\" WHERE username='"+ username +"'",{})
        .then(
          (result)=>{
            console.log( JSON.stringify(result.rows.item(0)) );
            console.log( JSON.stringify(result.rows) );
            if( result.rows.length == 0 ){
              if(typeof callbackError == 'function'){
                callbackError();
              }
            }else{

              let hash = Md5.hashStr(password + 'detorresrc').toString();
              console.log(password + 'detorresrc' + ' => ' +  hash);
              if( hash == result.rows.item(0).password && result.rows.item(0).active == 1 ){
                if(typeof callbackSuccess == 'function'){
                  callbackSuccess(result.rows.item(0));
                } 
              }else{
                if(typeof callbackError == 'function'){
                  callbackError();
                }
              }

            }

          },
          (error) => {
            console.log( JSON.stringify(error) );
            callbackError();
          }
        );

  }

}
