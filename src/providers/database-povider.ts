import {
  Platform,
  Events
} from 'ionic-angular';
import { Injectable } from '@angular/core';

import { SystemSettings } from '../providers/system-settings';
import { SQLite } from 'ionic-native';

@Injectable()
export class DatabaseProvider {

  private db: SQLite = null;

  public connected: boolean = false;

  constructor(
    public platform: Platform,
    public systemSettings: SystemSettings,
    public events: Events
  ) {

    platform.ready().then(() => {
      this.connect()
        .then(()=>{
          console.log('Connected!');
        });;
    });

  }

  connect():Promise<any>{
    return new Promise( (resolve, reject) => {

      if( this.connected == true ){
        resolve(this.db);
      }else{
        console.log('Connecting..');
        
        this.db = new SQLite();
        this.db.openDatabase(this.systemSettings.databaseOptions)
          .then(
            () => {
              this.events.publish('database:connected', {});

              this.connected = true;
              resolve(this.db);
            },
            (err) => {
              reject(false);
            }
          );
      }

    });

  }

  getDb():SQLite{
    return this.db;
  }

  isConnected(){
    return this.connected;
  }

}
