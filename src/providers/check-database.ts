import { Injectable } from '@angular/core';

import { SQLite } from 'ionic-native';

@Injectable()
export class CheckDatabase {

  constructor() {
  }

  check(){

    return new Promise( (resolve, reject) => {

      let db = new SQLite();
      db.openDatabase({
        name: 'eyetime.db',
        location: 'default'
      })
      .then(() => {
        db.executeSql('SELECT * FROM users', {})
          .then(
            () => {
              resolve(true);
            },
            (err) => {
              if(err.code === 5){
                reject('User table not found!');
              }else{
                reject(err.message);
              }
          });
      }, (err) => {
        reject('Can\'t open table!');
      });

    });
  }

}
