import { Injectable } from '@angular/core';

import {
  TextToSpeech
} from 'ionic-native';

@Injectable()
export class SpeechProvider {

  constructor(

  ) {
  }

  speak(text:string):Promise<any>{

    return new Promise((resolve, reject)=>{
      TextToSpeech.speak(text)
        .then(
          () => {
            resolve(true);
          }
        )
        .catch((reason: any) => {
          reject(reason);
        });
    });
    
  }

}
