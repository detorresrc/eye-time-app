import { Injectable } from '@angular/core';

import {
  LoadingController,
  Loading
} from 'ionic-angular';

import {
  Geolocation,
  Geoposition
} from 'ionic-native';

import { SystemSettings } from './system-settings';

@Injectable()
export class GeoLocationProvider {

  constructor(
    public loadingCtrl: LoadingController,
    public systemSettings: SystemSettings
  ) {
  }

  getLocation(
    timeout:number=30,
    showLoading:boolean=true,
    successCallback?:(geoPosition:Geoposition)=>void,
    errorCallback?:(error:string)=>void
    ){
    timeout = timeout * 1000;

    let loader:Loading;
    if(showLoading){
      loader = this.loadingCtrl.create({
        content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please wait..
        </div>`
      });
      loader.present();
    }

    let setting: any = this.systemSettings.locationSettings;
    setting.timeout = timeout;

    Geolocation.getCurrentPosition(setting)
      .then(
        (position) => {

          let geoPosition: any = {};
          geoPosition.coords = {};
          
          if ('coords' in position) {

            if ('latitude' in position.coords) {
              geoPosition.coords.latitude = position.coords.latitude;
            }
            if ('longitude' in position.coords) {
              geoPosition.coords.longitude = position.coords.longitude;
            }
            if ('accuracy' in position.coords) {
              geoPosition.coords.accuracy = position.coords.accuracy;
            }
            if ('altitude' in position.coords) {
              geoPosition.coords.altitude = position.coords.altitude;
            }
            if ('altitudeAccuracy' in position.coords) {
              geoPosition.coords.altitudeAccuracy = position.coords.altitudeAccuracy;
            }
            if ('heading' in position.coords) {
              geoPosition.coords.heading = position.coords.heading;
            }
            if ('speed' in position.coords) {
              geoPosition.coords.speed = position.coords.speed;
            }
          }

          if ('timestamp' in position) {
            geoPosition.timestamp = position.timestamp;
          }
          if(showLoading) loader.dismiss();

          if(typeof successCallback == 'function'){
            successCallback(geoPosition);
          }
        },
        (error) => {
          if(showLoading) loader.dismiss();
          if(typeof errorCallback == 'function'){
            errorCallback(error);
          }
        }
      ).catch(
        (error) => {
          if(typeof errorCallback == 'function'){
            errorCallback('app error');
          }
        }
      );
  }

}
