import { Injectable } from '@angular/core';

import {
  LoadingController
} from 'ionic-angular';

import {
  NFC,
  Ndef
} from 'ionic-native';

import {Md5} from 'ts-md5/dist/md5';
import { SpeechProvider } from './speech-provider';

@Injectable()
export class NfcProvider {

  constructor(
    public loadingCtrl: LoadingController,
    public speechProvider: SpeechProvider
  ) {
    
  }

  checkNfc(showSetting:boolean=false){

    return new Promise((resolve, reject)=>{
      NFC.enabled()
        .then(
          () => {
            resolve(true);
          },
          (error) => {
            reject(error);
          }
        )
        .catch(error => {
          reject(error);
        });
    });
    
  }

  showSetting(){
    NFC.showSettings();
  }

  write(data:string, callbackSuccess?:()=>void, callbackError?:(error:string)=>void){

    this.speechProvider.speak('Please tap NFC Card');
    
    let status:boolean = false;

    let loader = this.loadingCtrl.create({
      content: `
      <div class="custom-spinner-container">
        <div class="custom-spinner-box"></div>
        Please tap NFC Card
      </div>`,
      duration: 10000
    });
    loader.present();

    let listener = NFC.addNdefListener().subscribe(nfcData => {

      let tagID = NFC.bytesToHexString( nfcData.tag.id );
      let hash = Md5.hashStr( data + tagID ).toString();
      
      let message = Ndef.textRecord(data);
      let messageHash = Ndef.textRecord( hash );
      
      NFC.write([message, messageHash])
        .then(
          () => {
            status = true;
            loader.dismiss();

            if(typeof callbackSuccess === 'function'){
              callbackSuccess();
            }
            
          },
          (error) => {
            loader.dismiss();
          }
          );
      
    });

    loader.onDidDismiss(() => {
      listener.unsubscribe();
      if(status!=true){
        if(typeof callbackError === 'function'){
          callbackError('Write error!');
        }
      }
    });

  }

  read(
    showLoader:boolean=true,
    timeout:number=10,
    callbackSuccess?:(data:string)=>void,
    callbackError?:(error:string)=>void
  ){
    this.speechProvider.speak('Please tap NFC Card');

    timeout = timeout * 1000;

    let status:boolean = false;
    let errorMessage:string='Read error!';

    let loader;
    if(showLoader){
      loader = this.loadingCtrl.create({
        content: `
        <div class="custom-spinner-container">
          <div class="custom-spinner-box"></div>
          Please tap NFC Card
        </div>`,
        duration: timeout
      });
      loader.present();
    }    

    let listener = NFC.addNdefListener().subscribe(nfcData => {

      let tagID = NFC.bytesToHexString( nfcData.tag.id );

      if( nfcData.tag.ndefMessage.length==2 ){
        
        let data = this.parseMessage(nfcData.tag.ndefMessage[0].payload);
        let hash = this.parseMessage(nfcData.tag.ndefMessage[1].payload);

        if( this.validateMessage(tagID, data, hash) == false ){
          status = false;
          errorMessage='Invalid data!';
          
          if(showLoader)loader.dismiss();  
        }else{
          status = true;
          if(showLoader) loader.dismiss();
          if(typeof callbackSuccess === 'function'){
            callbackSuccess(data);
          }
        }

      }else{
        errorMessage='Invalid data!';
        if(showLoader) loader.dismiss();
      }
      
    });

    loader.onDidDismiss(() => {
      listener.unsubscribe();
      if(status!=true){
        if(typeof callbackError === 'function'){
          callbackError(errorMessage);
        }
      }
    });
  }

  private parseMessage(ndefData):string{
    let val = NFC.bytesToString( ndefData );
    val = val.substring(3, (val.length));
    return val;
  }

  private validateMessage(tagid, data, hash):boolean{
    let _hash = Md5.hashStr( data + tagid ).toString();

    return ( _hash == hash );
  }

}
