import { Injectable } from '@angular/core';

import { DatabaseProvider } from './database-povider';
import { LoggerProvider } from './logger-provider';
import { SystemSettings } from './system-settings';

@Injectable()
export class SystemSettingInitializer {

  constructor(
    public loggerProvider: LoggerProvider,
    public dbProvider: DatabaseProvider,
    public systemSettings: SystemSettings
  ) {
    console.log('Hello SystemSettingInitializer Provider');
  }

  init():Promise<any>{
    return new Promise((resolve, reject)=>{

      if(this.systemSettings.initialized == true){
        resolve(true);
        return;
      }

      this.dbProvider.connect()
      .then(

        (db) => {

          db.executeSql(`SELECT * FROM "main"."settings"`, {})
            .then(
              res => {
                if( res.rows.length > 0 ){
                  let row = res.rows.item(0);
                  
                  this.systemSettings.settings.data_upload_retry = row.data_upload_retry,
                  this.systemSettings.settings.data_upload_api_url = row.data_upload_api_url;
                  this.systemSettings.settings.gps_timeout = row.gps_timeout;
                  this.systemSettings.settings.nfc_read_timeout = row.nfc_read_timeout;
                  this.systemSettings.settings.require_gps = row.require_gps;

                  resolve(true);
                  this.systemSettings.initialized = true;
                  this.loggerProvider.log('System Settings successfully initialized!');

                }
              },
              error => {
                this.systemSettings.loadDefault();
                resolve(false);
              }
            )

        },
        error => {
          this.systemSettings.loadDefault();
          resolve(false);
        }

      );

    });
  }

}
