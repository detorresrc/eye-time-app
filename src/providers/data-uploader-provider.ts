import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { DatabaseProvider } from './database-povider';
import { SystemSettings } from './system-settings';
import { LoggerProvider } from './logger-provider';

import moment from 'moment';

@Injectable()
export class DataUploaderProvider {

  public sessionData:any;
  public logData:any;

  public started:boolean = false;

  constructor(
    public http: Http,
    public dbProvider: DatabaseProvider,
    public systemSettings: SystemSettings,
    public loggerProvider: LoggerProvider
    ) {
  }

  start(){
    if( this.started == false ){
      this.started = true;
      this._start();
    }
  }

  _start(){
  
    setTimeout(
      ()=>{

        this.loggerProvider.log('DataUploaderProvider => Processing');

        
        this.getSessionData()
          .then(
            success => {
              return this.getLogsData();
            },
            error => {
              this.loggerProvider.log('Error 1');
              return Promise.reject(error);
            }
          )
          .then(
            success => {
              return this.process({
                sessionData: this.sessionData,
                logData: this.logData
              });
            },
            error => {
              this.loggerProvider.log('Error 2');
              return Promise.reject(error);
            }
          )
          .then(
            success => {
              return this.flagData();
            },
            error => {
              this.loggerProvider.log('Error 3');
              return Promise.reject(error);
            }
          )
          .then(
            success => {
              this.loggerProvider.log('DataUploaderProvider => Processing => success');
              this._start();
            },
            error => {
              this.loggerProvider.log('Error 4');
              this.loggerProvider.error(error);
              this._start();
            }
          ).catch(
            error => {
              this.loggerProvider.error(error);
              this._start();
            }
          );
      },
      (this.systemSettings.settings.data_upload_retry*1000)
    );
    
  }

  flagData():Promise<any>{
    this.loggerProvider.log("DataUploaderProvider => flagData");

    return new Promise((resolve, reject) => {

      if( this.sessionData.length == 0 && this.logData.length == 0 ){
        reject('No data to process.');
      }else{
        this.dbProvider.getDb()
          .transaction((trx)=>{

            for(let i=0; i<this.sessionData.length; i++){
              let timestamp:number = parseInt(moment().format('x'));

              trx.executeSql(`UPDATE "main".sessions SET flag=1, upload_timestamp=? WHERE id=?`, [ timestamp, this.sessionData[i].id ]);
            }

            for(let ii=0; ii<this.logData.length; ii++){
              let timestamp:number = parseInt(moment().format('x'));

              trx.executeSql(`UPDATE "main".logs SET flag=1, upload_timestamp=? WHERE id=?`, [ timestamp, this.logData[ii].id ]);
            }

          })
            .then(
              success => {
                resolve(true);
              },
              error => {
                reject(error);
              }
            );
      }

    });
  }

  getSessionData():Promise<any>{
    this.loggerProvider.log("DataUploaderProvider => getSessionData");

    return new Promise((resolve, reject)=>{
      this.dbProvider.getDb()
        .executeSql("SELECT * FROM \"main\".sessions WHERE flag=0 and action='end'",{})
          .then(
            (ret) => {

              let dataToProcess:any = [];
              for(let i=0; i<ret.rows.length; i++){
                dataToProcess.push(
                  ret.rows.item(i)
                );
              }

              this.sessionData = dataToProcess;
              resolve(dataToProcess);

            },
            error => {
              reject(error);
            }
          );
    });
  }

  getLogsData():Promise<any>{
    this.loggerProvider.log("DataUploaderProvider => getLogsData");

    return new Promise((resolve, reject)=>{
      this.dbProvider.getDb()
        .executeSql("SELECT Session.uuid AS session_uuid, Log.* FROM \"main\".logs Log INNER JOIN \"main\".sessions Session ON(Session.id=Log.session_id) WHERE Log.flag=0",{})
          .then(
            (ret) => {

              let dataToProcess:any = [];
              for(let i=0; i<ret.rows.length; i++){
                dataToProcess.push(
                  ret.rows.item(i)
                );
              }

              this.logData = dataToProcess; 
              resolve(dataToProcess);

            },
            error => {
              reject(error);
            }
          );
    });
  }

  process(data:any):Promise<any>{
    this.loggerProvider.log("DataUploaderProvider => process");

      return new Promise((resolve, reject)=>{

        if( this.sessionData.length == 0 && this.logData.length == 0 ){
            reject('No data to process.');
        }

        this.loggerProvider.log("DataUploaderProvider => Uploading to " + this.systemSettings.settings.data_upload_api_url);

        let body = JSON.stringify(data);

        this.http.post(this.systemSettings.settings.data_upload_api_url, body)
          .subscribe(
            data => {

              if( data.status == 200 ){
                resolve( data );
              }else{
                console.log('Uploading error!');
                console.error( JSON.stringify(data) );  
                reject( data.statusText );
              }
            },
            error => {
              reject( error )
            }
          );

      });
    }

}