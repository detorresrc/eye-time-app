import { Injectable } from '@angular/core';

import { Device } from 'ionic-native';

import { DatabaseProvider } from './database-povider';

import moment from 'moment';

import { UUID } from 'angular2-uuid';

@Injectable()
export class SessionModelProvider {

  public sessionStatus:any = {};

  constructor(
    public dbProvider: DatabaseProvider
  ) {
    
  }

  startDay(
    lat:string,
    long:string,
    gpsTimestamp:number
  ){

    return this._save(
      'start',
      lat,
      long,
      gpsTimestamp,
      UUID.UUID()
    );

  }

  endDay(
    lat:string,
    long:string,
    gpsTimestamp:number
  ){

    return this._save(
      'end',
      lat,
      long,
      gpsTimestamp
    );

  }

  private _save(
    action:string,
    lat:string,
    long:string,
    gpsTimestamp:number,
    uuid?:string
  ):Promise<any>{
    return new Promise((resolve, reject)=>{

      let timestamp:number = parseInt(moment().format('x'));

      if(action=='start'){

        this.dbProvider.getDb()
          .executeSql('INSERT INTO "main"."sessions"(phone_id, action, s_long, s_lat, s_gps_timestamp, s_entry_timestamp, s_entry_date, uuid)\
          VALUES(?,?,?,?,?,?,?,?)',
            [
              Device.uuid,
              action,
              long,
              lat,
              gpsTimestamp,
              timestamp,
              moment(this.sessionStatus.timestamp).format('YYYY-MM-DD'),
              uuid
            ]
          )
            .then(
              (result)=>{

                this.sessionStatus = {
                  id: result.insertId,
                  action: action,
                  s_long: long,
                  s_lat: lat,
                  s_gps_timestamp: gpsTimestamp,
                  s_entry_timestamp: timestamp,
                  s_entry_date: moment(this.sessionStatus.timestamp).format('YYYY-MM-DD')
                };

                this.sessionStatus.s_gps_timestamp_formated = moment(this.sessionStatus.s_gps_timestamp).format("MMM DD YYYY hh:mm:ss A");
                this.sessionStatus.s_entry_timestamp_formated = moment(this.sessionStatus.s_entry_timestamp).format("MMM DD YYYY hh:mm:ss A");

                resolve(result);
              },
              (error) => {
                reject(error);
              }
            );

      }else{

        this.dbProvider.getDb()
          .executeSql(`
          UPDATE "main"."sessions"
            SET action=?, e_long=?, e_lat=?, e_gps_timestamp=?, e_entry_timestamp=?, e_entry_date=?
          WHERE
            id=?
          `,
            [
              action,
              long,
              lat,
              gpsTimestamp,
              timestamp,
              moment(this.sessionStatus.timestamp).format('YYYY-MM-DD'),
              this.sessionStatus.id
            ]
          )
            .then(
              (result)=>{

                this.sessionStatus = {
                  action: action,
                  s_long: long,
                  s_lat: lat,
                  s_gps_timestamp: gpsTimestamp,
                  s_entry_timestamp: timestamp,
                  s_entry_date: moment(this.sessionStatus.timestamp).format('YYYY-MM-DD')
                };

                this.sessionStatus.s_gps_timestamp_formated = moment(this.sessionStatus.s_gps_timestamp).format("MMM DD YYYY hh:mm:ss A");
                this.sessionStatus.s_entry_timestamp_formated = moment(this.sessionStatus.s_entry_timestamp).format("MMM DD YYYY hh:mm:ss A");

                resolve(result);
              },
              (error) => {
                reject(error);
              }
            );

      }

      
    });
  }

  // Return string start or end
  status():Promise<any>{
    return new Promise((resolve, reject)=>{

      this.dbProvider.getDb()
        .executeSql('SELECT * FROM "main"."sessions" ORDER BY id DESC LIMIT 1',{})
          .then(
            (result)=>{

              if( result.rows.length == 0 ){
                resolve('start');
              }else{
                this.sessionStatus = result.rows.item(0);

                this.sessionStatus.s_gps_timestamp_formated = moment(this.sessionStatus.s_gps_timestamp).format("MMM DD YYYY hh:mm:ss A");
                this.sessionStatus.s_entry_timestamp_formated = moment(this.sessionStatus.s_entry_timestamp).format("MMM DD YYYY hh:mm:ss A");

                if( result.rows.item(0).action == 'start' ){
                  resolve('end');
                }else if( result.rows.item(0).action == 'end' ){
                  resolve('start');
                }else{
                  reject(false);
                }
              }

            },
            (error) => {
              reject(false);
            }
          );

    });
  }

  safeToStartDate():Promise<any>{
    return new Promise((resolve, reject)=>{

      let today =  moment().format("YYYY-MM-DD");

      this.dbProvider.getDb()
        .executeSql('SELECT * FROM "main"."sessions" WHERE s_entry_date=?',[ today ])
          .then(
            (result)=>{

              if( result.rows.length == 0 ){
                resolve( { status: true } );
              }else{
                resolve( { status: false, Session: result.rows.item(0) } );
              }

            },
            (error) => {
              reject(error);
            }
          );

    });
  }

}
