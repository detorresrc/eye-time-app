import { Injectable } from '@angular/core';

@Injectable()
export class SystemSettings {
  public fingerPrintConfig: any = {
    clientId: "eyetimeapp",
    disableBackup: true
  };

  public databaseOptions: any = {
    name: 'eyetime.db',
    location: 'default'
  };

  public databaseInitialized: boolean = false;

  public locationSettings: any = {
    enableHighAccuracy: true,
    maximumAge: 0
  };

  public allowedAppByFingerScan:boolean = true;

  // Default Values
  private dataUploaderRetyTime = 60;
  private dataUploadApiUrl = 'http://202.128.49.237/eyetime.php';
  private gpsTimeout = 15;
  private nfcReadTimeout = 15;
  private requireGps = 1;

  public initialized:boolean = false;
  public settings = {
    data_upload_retry: 0,
    data_upload_api_url:'',
    gps_timeout:0,
    nfc_read_timeout:0,
    require_gps:0
  };

  constructor(
  ){}

  loadDefault(){
    this.settings.data_upload_retry = this.dataUploaderRetyTime,
    this.settings.data_upload_api_url = this.dataUploadApiUrl;
    this.settings.gps_timeout = this.gpsTimeout;
    this.settings.nfc_read_timeout = this.nfcReadTimeout;
    this.settings.require_gps = this.requireGps;
  }
}
